package br.com.unifor.feiradasprofissoes.splash_screen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.R
import br.com.unifor.feiradasprofissoes.MainActivity
import android.content.Intent
import android.os.Handler
import android.view.WindowManager


class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        supportActionBar?.hide()
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        Handler().postDelayed({
            startActivity(Intent(baseContext, Long::class.java))
            finish()
        }, 5000)
    }
}
package br.com.unifor.feiradasprofissoes.splash_cursos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.R
import br.com.unifor.feiradasprofissoes.info_curso.InfoCursoAds
import br.com.unifor.feiradasprofissoes.info_curso.InfoCursoAutomacao
import br.com.unifor.feiradasprofissoes.info_curso.InfoCursoCienciaComp
import br.com.unifor.feiradasprofissoes.info_curso.InfoCursoEngenhariaComp
import kotlinx.android.synthetic.main.splash_cursos.*

class SplashCursos : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_cursos)

        ads_card.setOnClickListener {
            var intent = Intent(this, InfoCursoAds::class.java)
            startActivity(intent)
        }

        cienciaComp_card.setOnClickListener {
            var intent = Intent(this, InfoCursoCienciaComp::class.java)
            startActivity(intent)
        }

        engenhariaComp_card.setOnClickListener {
            var intent = Intent(this, InfoCursoEngenhariaComp::class.java)
            startActivity(intent)
        }

        automacao_card.setOnClickListener {
            var intent = Intent(this, InfoCursoAutomacao::class.java)
            startActivity(intent)
        }
    }
}
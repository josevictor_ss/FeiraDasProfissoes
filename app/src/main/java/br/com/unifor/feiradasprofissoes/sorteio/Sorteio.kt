package br.com.unifor.feiradasprofissoes.sorteio

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import br.com.unifor.feiradasprofissoes.R
import kotlinx.android.synthetic.main.sorteio.*
import java.io.File
import java.io.FileOutputStream
import java.util.*

class Sorteio : AppCompatActivity() {

    val REQUEST_PERM_WRITE_STORAGE = 102
    private val CAPTURE_PHOTO = 104
    internal var imagePath: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sorteio)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        take_photo.setOnClickListener{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this@Sorteio, arrayOf(Manifest.permission.CAMERA), 1)
                }

            }
            if (ActivityCompat.checkSelfPermission(applicationContext,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this@Sorteio,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERM_WRITE_STORAGE)

            } else {
                takePhotoByCamera()
            }
        }

        buttonFloat_share.setOnClickListener {
            if(imagePath != "") {
                this.shareIntent()
            }
        }
    }

    fun takePhotoByCamera(){
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, CAPTURE_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                CAPTURE_PHOTO -> {

                    val capturedBitmap = data?.extras!!.get("data") as Bitmap

                    saveImage(capturedBitmap)
                    imageView_photo.setImageBitmap(capturedBitmap)
                }


                else -> {
                }
            }

        }
    }

    private fun saveImage(finalBitmap: Bitmap) {

        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root + "/capture_photo")
        myDir.mkdirs()
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        val OutletFname = "ImageInstagram_$n.jpg"
        val file = File(myDir, OutletFname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
            imagePath = file.absolutePath
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()

        }
    }

    private fun shareIntent() {
        val instagramIntent = Intent(Intent.ACTION_SEND)
        instagramIntent.type = "image/*"
        val media = File(imagePath)
        val uri = Uri.fromFile(media)
        instagramIntent.putExtra(Intent.EXTRA_STREAM, uri)
        instagramIntent.setPackage("com.instagram.android")

        val packManager = packageManager
        val resolvedInfoList = packManager.queryIntentActivities(instagramIntent, PackageManager.MATCH_DEFAULT_ONLY)

        var resolved = false
        for (resolveInfo in resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.instagram.android")) {
                instagramIntent.setClassName(
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.name
                )
                resolved = true
                break
            }
        }
        if (resolved) {
            startActivity(instagramIntent)
        } else {
            Toast.makeText(this@Sorteio, "Aplicativo Instagram não instalado", Toast.LENGTH_LONG).show()
        }
    }
}
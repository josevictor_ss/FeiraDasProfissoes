package br.com.unifor.feiradasprofissoes.cadastro

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.MainActivity
import br.com.unifor.feiradasprofissoes.R
import br.com.unifor.feiradasprofissoes.login.Login
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.cadastro.*
import kotlinx.android.synthetic.main.login.*


class Cadastro: AppCompatActivity(), View.OnClickListener {

    private val TAG1 = "CreateAccountActivity"
    private val TAG = "FirebaseEmailPassword"
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cadastro)

        btn_create.setOnClickListener(this)

        mAuth = FirebaseAuth.getInstance()
    }

    private fun initialise ( ) {

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()

    }

    override fun onClick(view: View?) {
        val i = view!!.id

        if (i == R.id.btn_create) {
            createNewAccount(
                regName.text.toString(), regEmail.text.toString(), regPassword.text.toString(), regSchool.text.toString(), regType.text.toString(), regCity.text.toString()
            )
        }
    }

    private fun createNewAccount(
        name: String,
        email: String,
        password: String,
        school: String,
        type: String,
        city: String
    ) {
        Log.e(TAG, "createAccount:" + email)
        Log.e(TAG, "signIn:" + email)
        if (!validateForm(name, email, password, school, type, city)) {
            return
        }
        mAuth!!.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.e(TAG, "createAccount: Successo!")
                    Toast.makeText(applicationContext, "Usuario registrado com sucesso", Toast.LENGTH_LONG).show()


                    verifyEmail()
                    // update UI with the signed-in user's information
                    val user = mAuth!!.currentUser
                    val userId = mAuth!!.currentUser!!.uid
                    val ref = FirebaseDatabase.getInstance().getReference("/users/$userId")
                    val usernow = User(userId, regName.text.toString(),regEmail.text.toString(), regPassword.text.toString(), regSchool.text.toString(), regType.text.toString(), regCity.text.toString()
                    )
                    ref.setValue(usernow)
                        .addOnSuccessListener {
                            Log.d("RegisterActivity", "Saved user to firebase")
                        }



                    var intent = Intent(this, Login::class.java)
                    startActivity(intent)


                } else {
                    Log.e(TAG, "createAccount: Falha!", task.exception)
                    Toast.makeText(applicationContext, "Falha na Autenticação!", Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, Cadastro::class.java)
                    startActivity(intent)
                }


            }
    }

    class User(val uid: String, val name: String, val email: String, val password: String, val school: String, val type: String, val city: String )

    private fun verifyEmail() {
        val mUser = mAuth!!.currentUser;
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this@Cadastro,
                        "Verification email sent to " + mUser.getEmail(),
                        Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(TAG, "sendEmailVerification", task.exception)
                    Toast.makeText(this@Cadastro,
                        "Failed to send verification email.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }


    private fun validateForm(
        name: String,
        email: String,
        password: String,
        school: String,
        type: String,
        city: String
    ): Boolean {

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(applicationContext, "Digite o seu nome!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (name.length < 6) {
            Toast.makeText(applicationContext, "Digite o seu nome completo!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(applicationContext, "Digite o seu Email!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(applicationContext, "Digite a sua Senha!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (password.length < 6) {
            Toast.makeText(applicationContext, "Senha curta, digite pelo menos 6 caracteres!", Toast.LENGTH_SHORT)
                .show()
            return false
        }

        if (TextUtils.isEmpty(school)) {
            Toast.makeText(applicationContext, "Digite o nome da sua escola!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(type)) {
            Toast.makeText(applicationContext, "Digite em que tipo de escola você estuda!", Toast.LENGTH_SHORT)
                .show()
            return false
        }

        if (TextUtils.isEmpty(city)) {
            Toast.makeText(applicationContext, "Digite a sua cidade de residência!", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }


}
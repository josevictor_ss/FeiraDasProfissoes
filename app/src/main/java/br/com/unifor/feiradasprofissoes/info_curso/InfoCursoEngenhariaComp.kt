package br.com.unifor.feiradasprofissoes.info_curso

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.R

class InfoCursoEngenhariaComp : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_curso_engenharia)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
package br.com.unifor.feiradasprofissoes.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.R
import kotlinx.android.synthetic.main.login.*
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast
import android.util.Log
import android.text.TextUtils
import br.com.unifor.feiradasprofissoes.cadastro.Cadastro
import br.com.unifor.feiradasprofissoes.splash_cursos.SplashCursos


class Login : AppCompatActivity(), View.OnClickListener {


    private val TAG = "FirebaseEmailPassword"
    private var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        btn_email_sign_in.setOnClickListener(this)
        register_text_btn.setOnClickListener(this)
        mAuth = FirebaseAuth.getInstance()
    }


    override fun onClick(view: View?) {
        val i = view!!.id

        if (i == R.id.btn_email_sign_in) {
            signIn(edtEmail.text.toString(), edtPassword.text.toString())

        }

        if (i == R.id.register_text_btn) {
            var intent = Intent(this, Cadastro::class.java)
            startActivity(intent)

        }


    }

    private fun signIn(email: String, password: String) {
        Log.e(TAG, "signIn:" + email)
        if (!validateForm(email, password)) {
            return
        }

        mAuth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.e(TAG, "signIn: Success!")
                    Toast.makeText(applicationContext, "Logado com sucesso", Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, SplashCursos::class.java)
                    startActivity(intent)
                }else{
                    Toast.makeText(applicationContext, "Falha no login", Toast.LENGTH_SHORT).show()
                }

            }
    }


    private fun validateForm(email: String, password: String): Boolean {

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(applicationContext, "Digite Email!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(applicationContext, "Digite Senha!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (password.length < 6) {
            Toast.makeText(applicationContext, "Senha curta, digite pelo menos 6 caracteres!", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

}


package br.com.unifor.feiradasprofissoes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import br.com.unifor.feiradasprofissoes.login.Login
import br.com.unifor.feiradasprofissoes.splash_cursos.SplashCursos


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            val intent = Intent(this, SplashCursos::class.java)
            startActivity(intent)
        }
    }
}

package br.com.unifor.feiradasprofissoes.info_curso

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.unifor.feiradasprofissoes.R

class InfoCursoCienciaComp : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_curso_ciencia)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}